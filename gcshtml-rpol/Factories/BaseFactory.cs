﻿using gcshtml_rpol.Commands;
using gcshtml_rpol.Instances;
using gcshtml_rpol.Models;
using gcshtml_rpol.ViewModels;
using gcshtml_rpol.Views;

namespace gcshtml_rpol.Factories {
    public static class BaseFactory {
        public static BaseInstance Create() {
            OpenCommand openCommand = new OpenCommand();
            SaveCommand saveCommand = new SaveCommand();
            BaseModel model = new BaseModel();
            BaseViewModel viewModel = new BaseViewModel(model, openCommand.Command, saveCommand.Command);
            BaseView view = new BaseView();

            openCommand.ViewModel = viewModel;
            saveCommand.ViewModel = viewModel;
            view.DataContext = viewModel;

            return new BaseInstance(view, viewModel, model);
        }
    }
}
