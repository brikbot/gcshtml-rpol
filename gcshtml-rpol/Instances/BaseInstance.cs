﻿using gcshtml_rpol.Models;
using gcshtml_rpol.ViewModels;
using gcshtml_rpol.Views;

namespace gcshtml_rpol.Instances {
    public class BaseInstance {
        public BaseView View { get; private set; }

        public BaseViewModel ViewModel { get; private set; }

        public BaseModel Model { get; private set; }

        public BaseInstance(BaseView view, BaseViewModel viewModel, BaseModel model) {
            this.View = view;
            this.ViewModel = viewModel;
            this.Model = model;
        }
    }
}
