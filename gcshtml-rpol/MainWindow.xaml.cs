﻿using GCM.Converters;
using gcshtml_rpol.Factories;
using gcshtml_rpol.Instances;
using Microsoft.Win32;
using Shards.WPF.UI;
using System;
using System.IO;

namespace gcshtml_rpol {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : BorderlessWindow {
        public MainWindow() {
            InitializeComponent();

            /*
            string result = null;

            FileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();

            if (!String.IsNullOrEmpty(dialog.FileName)) {
                using (Stream input = new FileStream(dialog.FileName, FileMode.Open)) {
                    using (TextReader reader = new StreamReader(input)) {
                        result = GcshtmlToRpol.Convert(reader.ReadToEnd());
                    }
                }
            }

            if (result != null) {
                dialog = new SaveFileDialog();
                dialog.ShowDialog();

                if (!String.IsNullOrEmpty(dialog.FileName)) {
                    using (Stream output = new FileStream(dialog.FileName, FileMode.Create)) {
                        using (TextWriter writer = new StreamWriter(output)) {
                            writer.Write(result);
                        }
                    }
                }
            }
            */

            BaseInstance instance = BaseFactory.Create();
            this.Content = instance.View;
        }
    }
}
