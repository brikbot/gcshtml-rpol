﻿using gcshtml_rpol.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace gcshtml_rpol.ViewModels {
    public class BaseViewModel : INotifyPropertyChanged {

        #region Local Variables

        private readonly BaseModel model;

        #endregion

        #region Properties

        public string InputHTML {
            get {
                return this.model.InputHTML;
            }
            set {
                if (this.model.InputHTML != value) {
                    this.model.InputHTML = value;
                    OnPropertyChanged("InputHTML");
                }
            }
        }
        public string OutputHTML {
            get {
                return this.model.OutputHTML;
            }
            set {
                if (this.model.OutputHTML != value) {
                    this.model.OutputHTML = value;
                    OnPropertyChanged("OutputHTML");
                }
            }
        }

        public ICommand OpenCommand { get; private set; }

        public ICommand SaveCommand { get; private set; }

        #endregion

        #region Constructors

        public BaseViewModel(BaseModel model, ICommand openCommand, ICommand saveCommand) {
            this.model = model;
            this.SaveCommand = saveCommand;
            this.OpenCommand = openCommand;
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Event Handlers

        protected virtual void OnPropertyChanged(string propertyName) {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
