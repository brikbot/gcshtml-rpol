﻿
namespace gcshtml_rpol.Models {
    public class BaseModel {
        public string InputHTML { get; set; }
        public string OutputHTML { get; set; }
    }
}
