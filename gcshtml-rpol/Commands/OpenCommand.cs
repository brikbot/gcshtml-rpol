﻿using GCM.Converters;
using gcshtml_rpol.ViewModels;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;

namespace gcshtml_rpol.Commands {
    public class OpenCommand {
        private BaseViewModel viewModel;

        public RelayCommand Command { get; private set; }

        public BaseViewModel ViewModel {
            get {
                return viewModel;
            }
            set {
                if (viewModel != value) {
                    if (viewModel != null) {
                        viewModel.PropertyChanged -= this.OnViewModelPropertyChanged;
                    }

                    viewModel = value;

                    if (viewModel != null) {
                        viewModel.PropertyChanged += this.OnViewModelPropertyChanged;
                    }
                }
            }
        }

        public OpenCommand() {
            this.Command = new RelayCommand(this.ExecuteOpen);
        }

        public void ExecuteOpen(object unused) {
            FileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();

            if (!String.IsNullOrEmpty(dialog.FileName)) {
                using (Stream input = new FileStream(dialog.FileName, FileMode.Open)) {
                    using (TextReader reader = new StreamReader(input)) {
                        this.ViewModel.InputHTML = reader.ReadToEnd();
                        this.ViewModel.OutputHTML = GcshtmlToRpol.Convert(this.ViewModel.InputHTML);
                    }
                }
            }
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e) {
            this.Command.RaiseCanExecuteChanged();
        }
    }
}
