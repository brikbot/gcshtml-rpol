﻿using gcshtml_rpol.ViewModels;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;

namespace gcshtml_rpol.Commands {
    public class SaveCommand {
        private BaseViewModel viewModel;

        public RelayCommand Command { get; private set; }

        public BaseViewModel ViewModel {
            get {
                return viewModel;
            }
            set {
                if (viewModel != value) {
                    if (viewModel != null) {
                        viewModel.PropertyChanged -= this.OnViewModelPropertyChanged;
                    }

                    viewModel = value;

                    if (viewModel != null) {
                        viewModel.PropertyChanged += this.OnViewModelPropertyChanged;
                    }
                }
            }
        }

        public SaveCommand() {
            this.Command = new RelayCommand(this.ExecuteSave, this.CanSave);
        }

        public void ExecuteSave(object unused) {
            FileDialog dialog = new SaveFileDialog();
            dialog.ShowDialog();

            if (!String.IsNullOrEmpty(dialog.FileName)) {
                using (Stream output = new FileStream(dialog.FileName, FileMode.Create)) {
                    using (TextWriter writer = new StreamWriter(output)) {
                        writer.Write(this.ViewModel.OutputHTML);
                    }
                }
            }
        }

        public bool CanSave(object unused) {
            return !String.IsNullOrEmpty(this.ViewModel.OutputHTML);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e) {
            this.Command.RaiseCanExecuteChanged();
        }
    }
}
