﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace gcshtml_rpol.Controls {
    /// <summary>
    /// Interaction logic for HTMLBrowser.xaml
    /// </summary>
    public partial class HTMLBrowser : UserControl, INotifyPropertyChanged {

        public static DependencyProperty HTMLProperty = DependencyProperty.Register("HTML", typeof(string), typeof(HTMLBrowser));

        public string HTML {
            get {
                return (string)GetValue(HTMLProperty);
            }
            set {
                if ((string)GetValue(HTMLProperty) != value) {
                    SetValue(HTMLProperty, value);
                    this.Browser.NavigateToString(value);
                    OnPropertyChanged("HTML");
                }
            }
        }

        public HTMLBrowser() {
            InitializeComponent();
        }

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Event Handlers

        protected virtual void OnPropertyChanged(string propertyName) {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
