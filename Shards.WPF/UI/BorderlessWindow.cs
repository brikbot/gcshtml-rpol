﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shell;

namespace Shards.WPF.UI {
    public partial class BorderlessWindow : Window {
        public static DependencyProperty HasShadowProperty = DependencyProperty.Register("HasShadow", typeof(bool), typeof(BorderlessWindow));
        public static DependencyProperty HasGripPropery = DependencyProperty.Register("HasGrip", typeof(bool), typeof(BorderlessWindow));

        static BorderlessWindow() {
            Type ownerType = typeof(BorderlessWindow);
            DefaultStyleKeyProperty.OverrideMetadata(ownerType, new FrameworkPropertyMetadata(ownerType));
        }

        private ShadowController shadowController;

        private Button _minimize;
        private Button _maximize;
        private Button _close;

        public bool HasShadow {
            get {
                return (bool)GetValue(HasShadowProperty);
            }
            set {
                if ((bool)GetValue(HasShadowProperty) != value) {
                    SetValue(HasShadowProperty, value);

                    if (shadowController == null) {
                        shadowController = new ShadowController(this);
                    }

                    if (value) {
                        shadowController.Show();
                    }
                    else {
                        shadowController.FullHide();
                    }
                }
            }
        }

        public bool HasGrip {
            get {
                return (bool)GetValue(HasGripPropery);
            }
            set {
                if ((bool)GetValue(HasGripPropery) != value) {
                    SetValue(HasGripPropery, value);

                    if (value) {
                        this.ResizeMode = ResizeMode.CanResizeWithGrip;
                    } else {
                        this.ResizeMode = ResizeMode.CanResize;
                    }
                }
            }
        }

        public new Brush BorderBrush {
            get {
                return base.BorderBrush;
            }
            set {
                if (base.BorderBrush != value) {
                    if (this.shadowController != null) {
                        this.shadowController.FocusedShadowBrush = value;
                        base.BorderBrush = value;
                    }
                }
            }
        }

        public BorderlessWindow() {
            this.shadowController = new ShadowController(this);

            this.WindowStyle = WindowStyle.None;
            this.ResizeMode = ResizeMode.CanResizeWithGrip;

            WindowChrome.SetWindowChrome(this, new WindowChrome() {
                CaptionHeight = 24,
                GlassFrameThickness = new Thickness(0d),
                ResizeBorderThickness = new Thickness(5d),
                CornerRadius = new CornerRadius(0d)
            });
        }

        public override void OnApplyTemplate() {
            base.OnApplyTemplate();

            if (this.shadowController != null) {
                this.shadowController.FocusedShadowBrush = this.BorderBrush;
            }

            this._minimize = GetTemplateChild("PART_Minimize") as Button;
            this._maximize = GetTemplateChild("PART_Maximize") as Button;
            this._close = GetTemplateChild("PART_Close") as Button;

            if (this._minimize != null) {
                this._minimize.Click += Minimize;
            }

            if (this._maximize != null) {
                this._maximize.Click += Maximize;
            }

            if (this._close != null) {
                this._close.Click += Close;
            }
        }

        public void Minimize(object sender, RoutedEventArgs e) {
            this.WindowState = WindowState.Minimized;
        }

        public void Maximize(object sender, RoutedEventArgs e) {
            if (this.WindowState != WindowState.Maximized) {
                this.WindowState = WindowState.Maximized;
            } else {
                this.WindowState = WindowState.Normal;
            }
        }

        public void Close(object sender, RoutedEventArgs e) {
            this.Close();
        }

        public new void Close() {
            if (shadowController != null) {
                shadowController.Close();
            }

            base.Close();
        }
    }
}
