﻿using System;
using System.Runtime.InteropServices;

namespace Shards.WPF.UI.Win32 {
    public static class NativeMethods {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hwnd, uint Msg, IntPtr wParam, IntPtr lParam);
    }
}